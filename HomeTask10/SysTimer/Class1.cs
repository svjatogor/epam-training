﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.IO;

namespace SysTimer
{
    public class Clock
    {
        private Thread Th1;
        public event EventHandler<ClockEventArgs> Completed;
        private int time;
        private bool enabled = false;

        public Clock()
        {
            time = 0;
        }

        /// <summary>
        /// Запускает отсчёт времени
        /// </summary>
        public void Start()
        {
            if (!enabled)
            {
                Th1 = new Thread(ClockThread);
                enabled = true;
                Th1.Start(this);
            }
        }

        /// <summary>
        /// Устанавливает или получает время работы таймера
        /// </summary>
        public int Time
        {
            get { return time; }
            set 
            {
                if (time < 0) throw new ArgumentException("Передано отрицательное время");
                time = value * 1000;
            }
        }

        /// <summary>
        /// Функция для потока
        /// </summary>
        /// <param name="o">Передаваемый объект</param>
        private void ClockThread(object o)
        {
            Clock c = o as Clock;
            Thread.Sleep(c.time);
            if(Completed!=null) c.Completed(c, new ClockEventArgs("Время вышло"));
            c.enabled = false;
        }
    }

    /// <summary>
    /// Класс обработки аргументов
    /// </summary>
    public class ClockEventArgs : EventArgs
    {
        public string msg;

        public ClockEventArgs(string msg)
        {
            this.msg = msg;
        }
    }
}

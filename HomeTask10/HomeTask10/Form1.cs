﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SysTimer;

namespace HomeTask10
{
    public partial class Form1 : Form
    {
        Clock c1 = new Clock();

        public Form1()
        {
            InitializeComponent();
            c1.Completed += (object s, ClockEventArgs ex) => { MessageBox.Show(ex.msg); };
            c1.Completed += delegate(object s, ClockEventArgs ex) { textBox2.Text = ex.msg; };
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                c1.Time = Convert.ToInt32(textBox1.Text);
                c1.Start();
            }
            catch(Exception exp)
            {
                MessageBox.Show(exp.Message,"Произошла ошибка");
                for(int i=0;i<40;i++) Console.Beep();
            }

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConvertBinary
{
    public static class Converter
    {
        public static string ToBinaryString(int number)
        {
            if (number < 0) throw new ArgumentException("Передано отрицательное число");

            StringBuilder s = new StringBuilder();

            while(number!=0)
            {
                s.Append((number % 2).ToString());
                number /= 2;
            }

            string str = s.ToString();
            return new string(str.Reverse().ToArray());
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SqrtNamespace;
using System.Windows.Forms;
using System.Drawing;

namespace HomeTask2
{
    class Program : Form
    {
        private TextBox number = new TextBox();
        private TextBox stepen = new TextBox();
        private Button calculate = new Button();
        private Label numberMsg = new Label();
        private Label stepenMsg = new Label();

        static void Main(string[] args)
        {
            Application.Run(new Program());
        }

        private Program()
        {
            this.Text = "HomeTask2";
            this.Width = 185;
            this.Height = 185;
            this.MaximumSize = new Size(185,185);

            numberMsg.Size = new Size(150, 20);
            numberMsg.Text = "Число:";
            numberMsg.Location = new Point(10, 10);
            this.Controls.Add(numberMsg);

            number.Size = new Size(150, 50);
            number.Text = "";
            number.Location = new Point(10, 30);
            this.Controls.Add(number);

            stepenMsg.Size = new Size(150, 20);
            stepenMsg.Text = "Степень корня:";
            stepenMsg.Location = new Point(10, 60);
            this.Controls.Add(stepenMsg);

            stepen.Size = new Size(150, 50);
            stepen.Text = "";
            stepen.Location = new Point(10, 80);
            this.Controls.Add(stepen);

            calculate.Size = new Size(150,30);
            calculate.Text = "Вычислить";
            calculate.Location = new Point(10,110);
            calculate.Click += new EventHandler(calculate_Clicked);
            this.Controls.Add(calculate);
        }

        private void calculate_Clicked(object o, EventArgs e)
        {
            try
            {
                double num = Convert.ToDouble(number.Text);
                double step = Convert.ToDouble(stepen.Text);

                MessageBox.Show("Корень степени " + stepen.Text + " из " + number.Text + " равен " + Sqrt.Calculate(num,step,0.00001)+"\nРезультат функции Math: "+Math.Pow(num,1/step), "Результат вычисления");
            }
            catch(Exception exp)
            {
                MessageBox.Show(exp.Message,"Ошибка");
            }
        }
    }
}

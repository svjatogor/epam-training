﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SqrtNamespace
{
    public static class Sqrt
    {
        private static double number;
        private static double stepen;
        private static double exp;

        private static double func(double x)
        {
            return Math.Pow(x, stepen) - number;
        }

        private static double proizv(double x)
        {
            return stepen * Math.Pow(x,stepen);
        }

        static public double Calculate(double number, double stepen = 2, double exp = 0.001)
        {
            if (number < 0 || stepen < 0 || (stepen > 0 && stepen < 0.001)) throw new ArgumentException("Степень корня или аргумент не соответствуют ограничениям");
            Sqrt.number = number;
            Sqrt.stepen = stepen;
            Sqrt.exp = exp;

            double x0 = 0, x = 1;

            while(Math.Abs(x-x0)>exp)
            {
                x0 = x;
                x = x0 - func(x0) / proizv(x0);
            }

            return x;
        }
    }
}

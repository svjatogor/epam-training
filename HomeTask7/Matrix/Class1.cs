﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Matrix
{
    public class MyMatrix
    {
        private double[,] matrix;

        public MyMatrix(int xSize, int ySize)
        {
            if (xSize < 1 || ySize < 1) throw new ArgumentException("Данная размерность матрицы невозможна");
            matrix = new double[xSize, ySize];
        }

        /// <summary>
        /// Содержимое матрицы
        /// </summary>
        public double[,] Data
        {
            get
            {
                return (double[,])matrix.Clone();
            }
            set
            {
                matrix = (double[,])value.Clone();
            }
        }

        /// <summary>
        /// Устанавливает или получает значение элемента матрицы
        /// </summary>
        /// <param name="iIndex">Индекс элемента (строка)</param>
        /// <param name="jIndex">Индекс элемента (столбец)</param>
        /// <returns></returns>
        public double this[int iIndex, int jIndex]
        {
            get
            {
                if (iIndex < 0 || jIndex < 0 || iIndex >= matrix.GetLength(0) || jIndex >= matrix.GetLength(1)) throw new ArgumentOutOfRangeException("Нет такого элемента в матрице");
                return matrix[iIndex, jIndex];
            }
            set
            {
                if (iIndex < 0 || jIndex < 0 || iIndex >= matrix.GetLength(0) || jIndex >= matrix.GetLength(1)) throw new ArgumentOutOfRangeException("Нет такого элемента в матрице");
                matrix[iIndex, jIndex] = value;                
            }
        }

        /// <summary>
        /// Получает размерность матрицы в выбранном измерении
        /// </summary>
        /// <param name="dimension">Измерение (0 - строки, 1 - столбцы)</param>
        /// <returns></returns>
        public int GetLength(int dimension)
        {
            switch(dimension)
            {
                case 0:
                    return matrix.GetLength(0);
                break;
                case 1:
                    return matrix.GetLength(1);
                break;
            }

            return 0;
        }

        /// <summary>
        /// Сложение двух матриц
        /// </summary>
        /// <param name="firstMatrix">Первая матрица</param>
        /// <param name="secondMatrix">Вторая матрица</param>
        /// <returns></returns>
        public static MyMatrix operator +(MyMatrix firstMatrix, MyMatrix secondMatrix)
        {
            if (firstMatrix.Equals(null) || secondMatrix.Equals(null)) throw new ArgumentNullException("Передан null");
            if (firstMatrix.GetLength(0) != secondMatrix.GetLength(0) || firstMatrix.GetLength(1) != secondMatrix.GetLength(1)) throw new ArgumentException("Размерность матриц отличается");
            MyMatrix newMatrix = new MyMatrix(1, 1);
            newMatrix.Data = firstMatrix.Data;

            for (int i = 0; i < newMatrix.GetLength(0); i++)
                for (int j = 0; j < newMatrix.GetLength(1); j++)
                {
                    newMatrix[i, j] += secondMatrix[i, j];
                }

            return newMatrix;
        }

        /// <summary>
        /// Вычитание двух матриц
        /// </summary>
        /// <param name="firstMatrix">Первая матрица</param>
        /// <param name="secondMatrix">Вторая матрица</param>
        /// <returns></returns>
        public static MyMatrix operator -(MyMatrix firstMatrix, MyMatrix secondMatrix)
        {
            if (firstMatrix.Equals(null) || secondMatrix.Equals(null)) throw new ArgumentNullException("Передан null");
            if (firstMatrix.GetLength(0) != secondMatrix.GetLength(0) || firstMatrix.GetLength(1) != secondMatrix.GetLength(1)) throw new ArgumentException("Размерность матриц отличается");
            MyMatrix newMatrix = new MyMatrix(1, 1);
            newMatrix.Data = firstMatrix.Data;

            for (int i = 0; i < newMatrix.GetLength(0); i++)
                for (int j = 0; j < newMatrix.GetLength(1); j++)
                {
                    newMatrix[i, j] -= secondMatrix[i, j];
                }

            return newMatrix;
        }

        /// <summary>
        /// Умножение двух матриц
        /// </summary>
        /// <param name="firstMatrix">Первая матрица</param>
        /// <param name="secondMatrix">Вторая матрица</param>
        /// <returns></returns>
        public static MyMatrix operator *(MyMatrix firstMatrix, MyMatrix secondMatrix)
        {
            if (firstMatrix.Equals(null) || secondMatrix.Equals(null)) throw new ArgumentNullException("Передан null");
            if (firstMatrix.GetLength(1) != secondMatrix.GetLength(0)) throw new ArgumentException("Размерность матриц отличается");
            MyMatrix newMatrix = new MyMatrix(firstMatrix.GetLength(0), secondMatrix.GetLength(1));
           
            for (int i = 0; i < newMatrix.GetLength(0); i++)
                for (int j = 0; j < newMatrix.GetLength(1); j++)
                {
                    for (int k = 0; k < firstMatrix.GetLength(1); k++) newMatrix[i, j] += firstMatrix[i, k] * secondMatrix[k, j];
                }

            return newMatrix;
        }

        /// <summary>
        /// Сравнивает две матрицы
        /// </summary>
        /// <param name="firstMatrix">Первая матрица</param>
        /// <param name="secondMatrix">Вторая матрица</param>
        /// <returns></returns>
        public static bool operator ==(MyMatrix firstMatrix, MyMatrix secondMatrix)
        {
            if (firstMatrix.Equals(null) || secondMatrix.Equals(null)) return false;
            if (firstMatrix.GetLength(0) != secondMatrix.GetLength(0) || firstMatrix.GetLength(1) != secondMatrix.GetLength(1)) return false;

            int iSize = firstMatrix.GetLength(0);
            int jSize = firstMatrix.GetLength(1);

            for (int i = 0; i < iSize; i++)
                for (int j = 0; j < jSize; j++)
                    if (firstMatrix[i, j] != secondMatrix[i, j]) return false;

            return true;
        }

        public static bool operator !=(MyMatrix firstMatrix, MyMatrix secondMatrix)
        {
            return !(firstMatrix==secondMatrix);
        }

        public override bool Equals(object obj)
        {
            if (!(obj is MyMatrix)) return false;
            return this == obj as MyMatrix;
        }
    }
}

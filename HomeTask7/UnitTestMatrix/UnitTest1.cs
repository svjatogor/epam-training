﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Matrix;

namespace UnitTestMatrix
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            MyMatrix matrix1 = new MyMatrix(3,3);
            MyMatrix matrix2 = new MyMatrix(3,1);

            matrix1.Data = new double[,]
            {
                {1,2,3},
                {4,5,6},
                {7,8,9}
            };

            matrix2.Data = new double[,]
            {
                {1},
                {2},
                {3}
            };

            MyMatrix expected = new MyMatrix(3,1);

            expected.Data = new double[,]
            {
                {14},
                {32},
                {50}
            };

            MyMatrix actual = matrix1 * matrix2;

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestMethod2()
        {
            MyMatrix matrix1 = new MyMatrix(3, 3);
            MyMatrix matrix2 = new MyMatrix(3, 3);

            matrix1.Data = new double[,]
            {
                {1,2,3},
                {4,5,6},
                {7,8,9}
            };

            matrix2.Data = new double[,]
            {
                {1,4,6},
                {2,8,9},
                {3,7,5}
            };

            MyMatrix expected = new MyMatrix(3, 3);

            expected.Data = new double[,]
            {
                {2,6,9},
                {6,13,15},
                {10,15,14}
            };

            MyMatrix actual = matrix1 + matrix2;

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestMethod3()
        {
            MyMatrix matrix1 = new MyMatrix(3, 3);
            MyMatrix matrix2 = new MyMatrix(3, 3);

            matrix1.Data = new double[,]
            {
                {1,2,3},
                {4,5,6},
                {7,8,9}
            };

            matrix2.Data = new double[,]
            {
                {1,4,6},
                {2,8,9},
                {3,7,5}
            };

            MyMatrix expected = new MyMatrix(3, 3);

            expected.Data = new double[,]
            {
                {0,-2,-3},
                {2,-3,-3},
                {4,1,4}
            };

            MyMatrix actual = matrix1 - matrix2;

            Assert.AreEqual(expected, actual);
        }
    }
}

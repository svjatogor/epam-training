﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Matrix;

namespace HomeTask7
{
    class Program
    {
        private static Random randValue = new Random();

        static void Main(string[] args)
        {
            Console.Write("Введите размерность первой матрицы (два числа через пробел): ");
            string razm = Console.ReadLine();
            Console.Write("Введите размерность второй матрицы (два числа через пробел): ");
            string razm2 = Console.ReadLine();

            string[] razmFirst = razm.Split(' ');
            string[] razmSecond = razm2.Split(' ');
            int[] FirstSize = {Convert.ToInt32(razmFirst[0]),Convert.ToInt32(razmFirst[1])};
            int[] SecondSize = {Convert.ToInt32(razmSecond[0]), Convert.ToInt32(razmSecond[1])};


            MyMatrix matrix1 = new MyMatrix(FirstSize[0],FirstSize[1]);
            MyMatrix matrix2 = new MyMatrix(SecondSize[0],SecondSize[1]);

            //Заполняем две матрицы случайными числами и выводим на экран
            GenMatrix(ref matrix1);
            GenMatrix(ref matrix2);
            ShowMatrix(matrix1);
            ShowMatrix(matrix2);

            try
            {
                MyMatrix matrix3 = matrix1 + matrix2;
                Console.WriteLine("Результаты сложения:\n");
                ShowMatrix(matrix3);
            }
            catch(Exception e)
            {
                Console.WriteLine("Произошла ошибка: "+e.Message);
            }

            try
            {
                MyMatrix matrix3 = matrix1 - matrix2;
                Console.WriteLine("Результаты вычитания:\n");
                ShowMatrix(matrix3);
            }
            catch (Exception e)
            {
                Console.WriteLine("Произошла ошибка: " + e.Message);
            }

            try
            {
                MyMatrix matrix3 = matrix1 * matrix2;
                Console.WriteLine("Результаты умножения:\n");
                ShowMatrix(matrix3);
            }
            catch (Exception e)
            {
                Console.WriteLine("Произошла ошибка: " + e.Message);
            }

        }

        public static void GenMatrix(ref MyMatrix matrix)
        {
            int iSize = matrix.GetLength(0);
            int jSize = matrix.GetLength(1);

            for (int i = 0; i < iSize; i++)
                for (int j = 0; j < jSize; j++)
                {
                    matrix[i, j] = randValue.Next(-100, 100) + randValue.NextDouble();
                }
        }

        public static void ShowMatrix(MyMatrix matrix)
        {
            int iSize = matrix.GetLength(0);
            int jSize = matrix.GetLength(1);

            for (int i = 0; i < iSize; i++)
            {
                for (int j = 0; j < jSize; j++)
                {
                    Console.Write("{0,8} ", Math.Round(matrix[i, j],3));
                }
                Console.WriteLine();
            }

            Console.WriteLine("-----------------");
        }
    }
}

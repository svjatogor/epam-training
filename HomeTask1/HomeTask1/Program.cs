﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using System.Text.RegularExpressions;
using ProcessorLibrary;

namespace HomeTask1
{
    class Program : Form
    {
        private TextBox inputNums = new TextBox();
        private Button convNumsBtn = new Button();
        private TextBox outputNums = new TextBox();
        private Button cancelBtn = new Button();

        /// <summary>
        /// Параметры приложения
        /// </summary>
        private struct AppVals
        {
            /// <summary>
            /// Задаёт разделитель между двумя цифрами в выходной строке
            /// </summary>
            public static string splitterNew = "; ";
            /// <summary>
            /// Задаёт значение разделителя между целой и дробной частью числа (до изменения)
            /// </summary>
            public static char numSplitterOld = '.';
            /// <summary>
            /// Задаёт значение разделителя между целой и дробной частью числа (после изменения)
            /// </summary>
            public static char numSplitterNew = ',';
        }

        static void Main(string[] args)
        {
            Application.Run(new Program());
        }

        /// <summary>
        /// Конструктор главной формы
        /// </summary>
        public Program()
        {
            this.Width = 360;
            this.Height = 200;
            this.Text = "HomeTask 1";
            this.MaximumSize = new Size(360,200);

            inputNums.Size = new Size(150, 100);
            inputNums.Text = "";
            inputNums.Location = new Point(10, 10);
            inputNums.Multiline = true;
            this.Controls.Add(inputNums);

            convNumsBtn.Text = "Преобразовать";
            convNumsBtn.Size = new Size(150, 30);
            convNumsBtn.Location = new Point(10, 120);
            convNumsBtn.Cursor = Cursors.Hand;
            convNumsBtn.Click += new EventHandler(convNumsBtn_Clicked);
            this.Controls.Add(convNumsBtn);

            outputNums.Size = new Size(150, 100);
            outputNums.Text = "";
            outputNums.Location = new Point(180, 10);
            outputNums.Multiline = true;
            outputNums.ReadOnly = true;
            this.Controls.Add(outputNums);

            cancelBtn.Text = "Очистить";
            cancelBtn.Size = new Size(150, 30);
            cancelBtn.Location = new Point(180, 120);
            cancelBtn.Cursor = Cursors.Hand;
            cancelBtn.Click += new EventHandler(cancelBtn_Clicked);
            this.Controls.Add(cancelBtn);
        }

        /// <summary>
        /// Обработчик события Click по кнопке convNumsBtn 
        /// </summary>
        private void convNumsBtn_Clicked(object o, EventArgs e)
        {
            try
            {
                outputNums.Text = Processor.Convert(inputNums.Text,true);
            }
            catch(Exception er)
            {
                MessageBox.Show(er.Message, "Ошибка");
            }
        }

 
        /// <summary>
        /// Обработчик события Click по кнопке cancelBtn 
        /// </summary>
        private void cancelBtn_Clicked(object o, EventArgs e)
        {
            inputNums.Text = "";
            outputNums.Text = "";
        }
    }
}

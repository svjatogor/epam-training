﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace ProcessorLibrary
{
    public static class Processor
    {
        static Regex numRegExp = new Regex(@"(-)?\d+(\.\d+)?");
        static Regex strRegExp = new Regex(@"^(-)?\d+(\.\d+)?[, ]+(-)?\d+(\.\d+)?$");

        struct ProcData
        {
            public static char OldSeparator = '.';
            public static char NewSeparator = ',';
        }

        public static MatchCollection GetAllNumbers(string numberString)
        {
            if (numberString == null) throw new Exception("Ошибка. Передан null");
            return numRegExp.Matches(numberString);
        }

        public static bool IsValidString(string controlString)
        {
            if (controlString == "") throw new Exception("Пустая строка");
            if (controlString == null) throw new Exception("Ошибка. Передан null");

            return (strRegExp.IsMatch(controlString)) ? true : false;
        }

        public static string Convert(string convertedString, bool deConvert = false)
        {
            if (convertedString==null) throw new Exception("Неопределённая ошибка. Передан null");

            string[] stringArr;

            if(deConvert) stringArr = convertedString.Split('\n');
            else stringArr = new string[] {convertedString};

            string outStr = "";
            int stringNum = 0;

            foreach(string s in stringArr)
            {
                string k;

                stringNum++;
                
                if (s.Length!=0 && s[s.Length - 1] == '\r') k = s.Substring(0, s.Length - 1);
                else k = s;

                if(k.Length==0) continue;

                if (!IsValidString(k)) throw new Exception("Некорректная строка " + stringNum);

                MatchCollection col = GetAllNumbers(k);

                outStr += "X:" + col[0].Value.Replace(ProcData.OldSeparator, ProcData.NewSeparator) + "; Y:" + col[1].Value.Replace(ProcData.OldSeparator, ProcData.NewSeparator) + "\r\n";
            }

            if (outStr.Length == 0) throw new Exception("Строка результата пуста");

            return outStr;
        }
    }
}

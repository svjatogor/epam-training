﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProcessorLibrary;

namespace ProcessorTest
{
    [TestClass]
    public class ProcessorTests
    {
        [TestMethod]
        public void TestMethod1()
        {
            string testStr = "123,456";

            bool actual = Processor.IsValidString(testStr);
            bool expected = true;

            Assert.AreEqual(expected, actual, "Неверный ответ");
        }

        [TestMethod]
        public void TestMethod2()
        {
            string testStr = "123, 456";

            bool actual = Processor.IsValidString(testStr);
            bool expected = true;

            Assert.AreEqual(expected, actual, "Неверный ответ");
        }

        [TestMethod]
        public void TestMethod3()
        {
            string testStr = "123.456";

            bool actual = Processor.IsValidString(testStr);
            bool expected = false;

            Assert.AreEqual(expected, actual, "Неверный ответ");
        }

        [TestMethod]
        public void TestMethod4()
        {
            string testStr = "blabla";

            bool actual = Processor.IsValidString(testStr);
            bool expected = false;

            Assert.AreEqual(expected, actual, "Неверный ответ");
        }

        [TestMethod]
        public void TestMethod5()
        {
            string testStr = "-123.456, 786";

            bool actual = Processor.IsValidString(testStr);
            bool expected = true;

            Assert.AreEqual(expected, actual, "Неверный ответ");
        }
    }
}

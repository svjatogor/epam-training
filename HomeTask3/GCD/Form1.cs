﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using HomeTask3;

namespace Project
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            string[] X = new string[2] { "Евклид", "Стейн"};
            double[] Y = new double[2] { 1,2 };

            chart1.Series[0].Points.DataBindXY(X, Y);
        }

        private void calculateButton_Click(object sender, EventArgs e)
        {
            try
            {
                int[] nums = inputData.Text.GetNums();
                int time1 = 0, time2 = 0;

                outputData.Text = GCD.Calculate(out time1,nums).ToString();
                outputData2.Text = GCD.BinaryGCD(out time2, nums).ToString();

                string[] X = new string[2] { "Евклид", "Стейн" };
                double[] Y = new double[2] { time1, time2 };

                chart1.Series[0].Points.DataBindXY(X, Y);
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message,"Произошла ошибка");
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            inputData.Text = "";
            outputData.Text = "";
            outputData2.Text = "";
        }
    }
}

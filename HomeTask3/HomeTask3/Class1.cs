﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.Threading;
using System.Diagnostics;

namespace HomeTask3
{
    public static class GCD
    {
        public static int Calculate(int a, int b)
        {
            a = Math.Abs(a);
            b = Math.Abs(b);

            while(a!=0 && b!=0)
            {
                if(a>b) a %= b;
                else b %= a;
            }

            return b==0?a:b;
        }

        public static int Calculate(out int time, params int[] nums)
        {
            if (nums.Length < 2) throw new ArgumentException("Неверное количество параметров в Calculate");

            Stopwatch procTime = new Stopwatch();
            procTime.Start();

            int gcd = Calculate(Math.Abs(nums[0]), Math.Abs(nums[1]));

            for(int i=2;i<nums.Length;i++)
            {
                gcd = Calculate(gcd,nums[i]);
            }

            procTime.Stop();
            time = Convert.ToInt32(procTime.ElapsedTicks);

            return gcd;
        }

        public static int BinaryGCD(int a, int b)
        {
            if (a == b || b == 0) return a; //a равно b или b равно 0
            if (a == 0) return b; //a равно 0
            if (a == 1 || b == 1) return 1; //одно из чисел равно 1
            if (a % 2 == 0)
                if (b % 2 == 0) return 2 * BinaryGCD(a / 2, b / 2); //a и b чётные
                else return BinaryGCD(a / 2, b); //a чётное, b нечётное
            else
                if (b % 2 == 0) return BinaryGCD(a,b/2); //a нечётное, b чётное

            return BinaryGCD(Math.Abs(a-b)/2,(a<b)?a:b); //оба нечётные, a>b или a<b 
        }

        public static int BinaryGCD(out int time, params int[] nums)
        {
            if (nums.Length < 2) throw new ArgumentException("Неверное количество параметров в BinaryGCD");

            Stopwatch procTime = new Stopwatch();
            procTime.Start();

            int gcd = BinaryGCD(Math.Abs(nums[0]), Math.Abs(nums[1]));

            for (int i = 2; i < nums.Length; i++)
            {
                gcd = Calculate(gcd, nums[i]);
            }

            procTime.Stop();
            time = Convert.ToInt32(procTime.ElapsedTicks);

            return gcd;
        }

        public static int[] GetNums(this String inpS)
        {
            String[] s = inpS.Split(',', ' ');

            int[] nums = new int[s.Length];
            int i = 0;

            foreach (String buf in s)
            {
                nums[i] = Convert.ToInt32(buf.Trim());
                i++;
            }

            return nums;
        }
    }
}

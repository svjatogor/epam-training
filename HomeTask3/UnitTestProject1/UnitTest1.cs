﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using HomeTask3;

namespace UnitTestProject1
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            int expected = 21;
            int actual = GCD.Calculate(1071, 462);
            Assert.AreEqual(expected, actual, "Неверно найден НОД");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestMethod2()
        {
            int time;
            int actual = GCD.Calculate(out time, 462);
        }

        [TestMethod]
        public void TestMethod3()
        {
            int expected = 21;
            int actual = GCD.BinaryGCD(1071, 462);
            Assert.AreEqual(expected, actual, "Неверно найден НОД");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UML;

namespace UML_Demo
{
    class Program
    {
        static void Main(string[] args)
        {
            Client cl1 = new Client(new Factory1());
            Client cl2 = new Client(new Factory2());

            Console.WriteLine(cl1.product1.ProduceA());
            Console.WriteLine(cl1.product2.ProduceB());
            Console.WriteLine(cl2.product1.ProduceA());
            Console.WriteLine(cl2.product2.ProduceB());

        }
    }
}

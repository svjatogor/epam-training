﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BTree
{
    public struct TreeElem : IComparable, ICloneable
    {
        public string StudentName;
        public string TestTitle;
        public int Mark;

        /// <summary>
        /// Сравнивает текущий объект с другим объектом
        /// </summary>
        /// <param name="obj">Объект для сравнения</param>
        /// <returns></returns>
        public int CompareTo(object obj)
        {
            if (obj == null) throw new ArgumentNullException("Передан null");
            return Mark.CompareTo(((TreeElem)obj).Mark);
        }

        /// <summary>
        /// Создаёт неполную копию текущего объекта 
        /// </summary>
        /// <returns>Неполная копия текущего объекта</returns>
        public object Clone()
        {
            return MemberwiseClone();
        }

        /// <summary>
        /// Сравнивает два объекта TreeElem
        /// </summary>
        /// <param name="t1">Первый оъект для сравнения</param>
        /// <param name="t2">Второй объект для сравнения</param>
        /// <returns>Возвращает результат сравнения (true или false)</returns>
        public static bool operator ==(TreeElem t1, TreeElem t2)
        {
            if (t1.Mark != t2.Mark) return false;
            if (t1.StudentName != t2.StudentName) return false;
            if (t1.TestTitle != t2.TestTitle) return false;
            return true;
        }

        public static bool operator !=(TreeElem t1, TreeElem t2)
        {
            return !(t1 == t2);
        }

        /// <summary>
        /// Сравнивает текущий объект с другим объектом
        /// </summary>
        /// <param name="obj">Объект для сравнения</param>
        /// <returns>Возвращает результат сравнения (true или false)</returns>
        public override bool Equals(object obj)
        {
            if (!(obj is TreeElem)) throw new ArgumentException();
            if (obj == null) return false;
            return this == (TreeElem)obj;
        }

        public override int GetHashCode()
        {
            return (StudentName+"|"+TestTitle+"|"+Mark).GetHashCode();
        }

        public override string ToString()
        {
            return String.Format("ФИО: {0}, Название теста: {1}, Оценка: {2}",StudentName,TestTitle,Mark);
        }
    }

    public class BinaryTree<Type> : ICollection<Type> where Type : IComparable, ICloneable
    {
        private BTreeElem<Type> root;
        private int size;

        /// <summary>
        /// Создаёт новый объект BinaryTree
        /// </summary>
        public BinaryTree()
        {
            root = null;
            size = 0;
        }

        /// <summary>
        /// Создаёт новый объект BinaryTree и добавляет в него элемент
        /// </summary>
        /// <param name="item">Добавляемый элемент</param>
        public BinaryTree(Type item) : this()
        {
            if (item == null) throw new ArgumentNullException("Передан null");
            this.Add(item);
        }

        /// <summary>
        /// Добавляет элемент в коллекцию BinaryTree
        /// </summary>
        /// <param name="item">Добавляемый элемент</param>
        public void Add(Type item)
        {
            if (item == null) throw new ArgumentNullException("Передан null");

            size++;

            BTreeElem<Type> cur = root;
            BTreeElem<Type> newElem = new BTreeElem<Type>(item);

            if (cur == null)
            {
                root = newElem;
            }
            else
            {
                while (true)
                {
                    if (cur.Data.CompareTo(item) <= 0)
                    {
                        if (cur.Right == null) { cur.Right = newElem; break; }
                        else cur = cur.Right;
                    }
                    else
                    {
                        if (cur.Left == null) { cur.Left = newElem; break; }
                        else cur = cur.Left;
                    }
                }
            }
        }

        /// <summary>
        /// Удаляет все элементы из BinaryTree
        /// </summary>
        public void Clear()
        {
            root = null;
            size = 0;
        }

        /// <summary>
        /// Проверяет вхождение элемента в BinaryTree
        /// </summary>
        /// <param name="item">Элемент, вхождение которого нужно проверить</param>
        /// <returns>true, если элемент найден, иначе false</returns>
        public bool Contains(Type item)
        {
            if (item == null) throw new ArgumentNullException("Передан null");

            BTreeElem<Type> cur = root;

            while(true)
            {
                switch(cur.Data.CompareTo(item))
                {
                    case 0:
                        return true;

                    case 1:
                        if (cur.Left == null) return false;
                        else cur = cur.Left;
                    break;
                    
                    case -1:
                        if (cur.Right == null) return false;
                        else cur = cur.Right;
                    break;
                }
            }
        }

        /// <summary>
        /// Копирует все элементы BinaryTree в массив
        /// </summary>
        /// <param name="array">Массив, в который производится копирование</param>
        /// <param name="arrayIndex">Индекс, с которого производится копирование</param>
        public void CopyTo(Type[] array, int arrayIndex)
        {
            if (array == null) throw new ArgumentNullException("Передан null");
            if(arrayIndex + size > array.Length || arrayIndex < 0) throw new ArgumentOutOfRangeException("Выход за пределы массива");
            
            int finalIndex = arrayIndex + size;
            int i = arrayIndex;

            foreach(Type item in this)
            {
                array[i] = item;
                i++;
            }
        }

        /// <summary>
        /// Возвращает количество элементов в BinaryTree
        /// </summary>
        public int Count
        {
            get { return size; }
        }

        /// <summary>
        /// Проверяет, доступен ли элемент только для чтения
        /// </summary>
        public bool IsReadOnly
        {
            get { return false; }
        }

        /// <summary>
        /// Удаляет первое вхождение указанного объекта из коллекции BinaryTree
        /// </summary>
        /// <param name="item">Объект, который необходимо удалить из BinaryTree</param>
        /// <returns>Значение true, если элемент item успешно удален, в противном случае — значение false. Этот метод также возвращает false, если элемент item не найден в коллекции BinaryTree</returns>
        public bool Remove(Type item)
        {
            if (item == null) throw new ArgumentNullException("Передан null");

            BTreeElem<Type> cur = root, parent = null, tmpParent = null, tmpCur = null;

            while (true)
            {
                switch (cur.Data.CompareTo(item))
                {
                    //Если найдено совпадение
                    case 0:
                        #region Если найдено совпадение

                        size--;

                        //Если у текущего нет правого листа, правым сыном родителя становится левый сын текущего 
                        if (cur.Right == null)
                        {
                            //Если удаляем не корневой элемент
                            if (parent != null)
                            {
                                //Если текущий является правым сыном
                                if (parent.Right == cur) parent.Right = cur.Left;
                                else parent.Left = cur.Left;
                            }
                            else root = cur.Left;
                        }
                        //Если у текущего нет левого листа, правым сыном родителя становится правый сын текущего
                        else if (cur.Left == null)
                        {
                            //Если удаляем не корневой элемент
                            if (parent != null)
                            {
                                //если текущий является правым сыном
                                if (parent.Right == cur) parent.Right = cur.Right;
                                else parent.Left = cur.Left;
                            }
                            else root = cur.Right;
                        }
                        else
                        {
                            //Устанавливаем начальные значения для поиска максимального из левого поддерева
                            tmpParent = cur;
                            tmpCur = cur.Left;

                            //Ищем максимальный из левого поддерева
                            while (tmpCur.Right != null) { tmpParent = tmpCur; tmpCur = tmpCur.Right; }

                            //Удаляем максимальный из листов своего бывшего родителя
                            if (tmpParent.Left == tmpCur) tmpParent.Left = null;
                            else tmpParent.Right = null;
                            //Назначаем максимальному нового родителя
                            if (parent != null)
                            {
                                if (parent.Right == cur) parent.Right = tmpCur;
                                else parent.Left = tmpCur;
                            }
                            else root = tmpCur;
                            //Восстанавливаем левое и правое поддерево для текущего элемента
                            tmpCur.Left = cur.Left;
                            tmpCur.Right = cur.Right;
                            //Уничтожаем объект
                            cur = null;
                        }
                        #endregion
                    return true;

                    case 1:
                        if (cur.Left == null) return false;
                        else { parent = cur; cur = cur.Left; }
                    break;

                    case -1:
                        if (cur.Right == null) return false;
                        else { parent = cur; cur = cur.Right; }
                    break;
                }
            }
        }

        public IEnumerator<Type> GetEnumerator()
        {
            Stack<BTreeElem<Type>> elems = new Stack<BTreeElem<Type>>();
            BTreeElem<Type> cur = root;

            while(elems.Count!=0 || cur!=null)
            {
                if(cur!=null)
                {
                    elems.Push(cur);
                    cur = cur.Left;
                }
                else
                {
                    cur = elems.Pop();
                    yield return cur.Data;
                    cur = cur.Right;
                }
            }
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            Stack<BTreeElem<Type>> elems = new Stack<BTreeElem<Type>>();
            BTreeElem<Type> cur = root;

            while (elems.Count != 0 || cur != null)
            {
                if (cur != null)
                {
                    elems.Push(cur);
                    cur = cur.Left;
                }
                else
                {
                    cur = elems.Pop();
                    yield return cur.Data;
                    cur = cur.Right;
                }
            }
        }
    }

    internal class BTreeElem<Type> where Type : IComparable, ICloneable
    {
        public Type Data;
        public BTreeElem<Type> Left, Right;

        public BTreeElem(Type element)
        {
            Left = Right = null;
            Data = (Type)element.Clone();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UML
{
    public class Client
    {
        public IProductA product1 { get; private set; }
        public IProductB product2 { get; private set; }

        public Client(IFactory factory)
        {
            product1 = factory.GetProductA();
            product2 = factory.GetProductB();
        }
    }
}

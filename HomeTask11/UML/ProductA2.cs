﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UML
{
    public class ProductA2 : IProductA
    {
        public string ProduceA()
        {
            return "Produce A2";
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BTree;

namespace TreeConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                TreeElem t1 = new TreeElem();
                BinaryTree<TreeElem> tree = new BinaryTree<TreeElem>();

                bool isWhile = true;

                while(isWhile)
                {
                    Console.WriteLine("Выберите действие: ");
                    Console.WriteLine("1) Добавить элемент");
                    Console.WriteLine("2) Найти элемент");
                    Console.WriteLine("3) Удалить элемент");
                    Console.WriteLine("4) Показать все");
                    Console.WriteLine("5) Количество элементов");
                    
                    ConsoleKeyInfo k = Console.ReadKey();
                    Console.WriteLine();
                    
                    switch(k.KeyChar)
                    {
                        case '1':
                            Console.WriteLine("Введите информацию о студенте:");
                            Console.Write("Имя: ");
                            t1.StudentName = Console.ReadLine();
                            Console.Write("Название теста: ");
                            t1.TestTitle = Console.ReadLine();
                            Console.Write("Оценка: ");
                            t1.Mark = Convert.ToInt32(Console.ReadLine());

                            tree.Add(t1);

                            Console.WriteLine("Элемент успешно добавлен");

                            break;

                        case '2':
                            Console.WriteLine("Введите элемент для поиска:");
                            Console.Write("Имя: ");
                            t1.StudentName = Console.ReadLine();
                            Console.Write("Название теста: ");
                            t1.TestTitle = Console.ReadLine();
                            Console.Write("Оценка: ");
                            t1.Mark = Convert.ToInt32(Console.ReadLine());

                            if (tree.Contains(t1)) Console.WriteLine("Элемент найден");
                            else Console.WriteLine("Элемент не найден");

                            break;

                        case '3':
                            Console.WriteLine("Введите элемент удаления:");
                            Console.Write("Имя: ");
                            t1.StudentName = Console.ReadLine();
                            Console.Write("Название теста: ");
                            t1.TestTitle = Console.ReadLine();
                            Console.Write("Оценка: ");
                            t1.Mark = Convert.ToInt32(Console.ReadLine());

                            if (tree.Remove(t1)) Console.WriteLine("Элемент удалён");
                            else Console.WriteLine("Элемент не найден");

                            break;

                        case '4':
                            Console.WriteLine("--------------------------------------------------------");
                            Console.WriteLine("| ФИО                | Название теста     | Оценка     |");
                            Console.WriteLine("|--------------------|--------------------|------------|");


                            foreach(TreeElem elem in tree)
                            {
                                Console.WriteLine("|{0,20}|{1,20}|{2,12}|",elem.StudentName,elem.TestTitle,elem.Mark);
                            }

                            Console.WriteLine("|--------------------|--------------------|------------|");

                            break;

                        case '5':
                            Console.WriteLine("В дереве {0} элементов",tree.Count);
                            break;
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Произошла ошибка: {0}",e.Message);
            }

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TriangleClass
{
    public class Triangle
    {
        private double x, y, z;

        public Triangle(params double[] input)
        {
            if (input == null) throw new ArgumentNullException("Передан null вместо параметра");
            if (input.Length > 3 && input.Length < 6) throw new ArgumentException("Неверное количество параметров");
        
            if(input.Length==3)
            {
                if (input[0] <= 0 || input[1] <= 0 || input[2] <= 0) throw new ArgumentException("Отрицательные длины сторон невозможны");

                this.x = input[0];
                this.y = input[1];
                this.z = input[2];
            }
            else
            {
                this.x = Math.Sqrt(Math.Pow(input[0] - input[2], 2) + Math.Pow(input[1] - input[3], 2));
                this.y = Math.Sqrt(Math.Pow(input[2] - input[4], 2) + Math.Pow(input[3] - input[5], 2));
                this.z = Math.Sqrt(Math.Pow(input[4] - input[0], 2) + Math.Pow(input[5] - input[1], 2));
            }

            if(x>=y+x || y>=x+z || z>=x+y) throw new ArgumentException("Треугольник не существует");
        }

        public double Perimetr()
        {
            return x + y + z;
        }

        public double Square()
        {
            double p = Perimetr()/2;
            return Math.Sqrt(p*(p-x)*(p-y)*(p-z));
        }

        public double getX
        {
            get { return x; }
        }

        public double getY
        {
            get { return y; }
        }

        public double getZ
        {
            get { return z; }
        }
    }
}

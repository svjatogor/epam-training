﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TriangleClass;

namespace UnitTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Unexist_triangle()
        {
            Triangle tr = new Triangle(1,2,3);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Num_of_params()
        {
            Triangle tr = new Triangle(1, 2, 3, 4, 5);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Null_exception()
        {
            Triangle tr = new Triangle(null);
        }

        [TestMethod]
        public void TestMethod1()
        {
            Triangle tr = new Triangle(5, 5, 5);

            double expected = 10.8253175473054830845465396;
            double actual = tr.Square();

            Assert.AreEqual(expected, actual, "Неверная площадь");
        }

        [TestMethod]
        public void TestMethod2()
        {
            Triangle tr = new Triangle(5, 5, 5);

            double expected = 15;
            double actual = tr.Perimetr();

            Assert.AreEqual(expected, actual, "Неверный периметр");
        }
    }
}

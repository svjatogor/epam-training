﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TriangleClass;

namespace HomeTask4
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs ex)
        {
            try
            {
                string[] inputArr = inputData.Text.Split(';');
                if (inputArr.Length>3 && inputArr.Length<6) throw new ArgumentException("Неверный формат ввода");

                Triangle tr;

                if(inputArr.Length==3)
                {
                    double x = Convert.ToDouble(inputArr[0].Trim());
                    double y = Convert.ToDouble(inputArr[1].Trim());
                    double z = Convert.ToDouble(inputArr[2].Trim());

                    tr = new Triangle(x,y,z);
                }
                else
                {
                    double a = Convert.ToDouble(inputArr[0].Trim());
                    double b = Convert.ToDouble(inputArr[1].Trim());
                    double c = Convert.ToDouble(inputArr[2].Trim());
                    double d = Convert.ToDouble(inputArr[3].Trim());
                    double e = Convert.ToDouble(inputArr[4].Trim());
                    double f = Convert.ToDouble(inputArr[5].Trim());

                    tr = new Triangle(a,b,c,d,e,f);
                }

                perimetrOut.Text = tr.Perimetr().ToString();
                squareOut.Text = tr.Square().ToString();
            }
            catch(Exception e)
            {
                MessageBox.Show(e.Message,"Ошибка");
            }
        }
    }
}

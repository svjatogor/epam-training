﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Polynom
{
    public class MyPolynom
    {
        private double[] coefficients;

        public MyPolynom(int stepen)
        {
            coefficients = new double[stepen];
        }

        public double this[int index]
        {
            get 
            {
                if(index<0 || index>=coefficients.Length) throw new ArgumentOutOfRangeException("Выход за пределы степени полинома");

                return coefficients[index];
            }

            set
            {
                if (index < 0 || index >= coefficients.Length) throw new ArgumentOutOfRangeException("Выход за пределы степени полинома");

                coefficients[index] = value;
            }
        }

        public double[] Data
        {
            set
            {
                Array.Copy(value, coefficients, value.Length);
            }

            get
            {
                double[] newArr;
                newArr = (double[])coefficients.Clone();
                return newArr;
            }
        }

        public int Length
        {
            get
            {
                return coefficients.Length;
            }
        }

        public double DependsOf(double x)
        {
            double value = 0;
            int i=0;

            foreach(double coef in coefficients)
            {
                value += coef * Math.Pow(x, i);
                i++;
            }

            return value;
        }

        public static bool operator !=(MyPolynom poly1, MyPolynom poly2)
        {
            if (poly1.Length != poly2.Length) return true;

            for (int i = 0; i < poly1.Length; i++)
            {
                if (poly1[i] != poly2[i]) return true;
            }

            return false;
        }

        public static bool operator ==(MyPolynom poly1, MyPolynom poly2)
        {
            return !(poly1 != poly2);
        }

        public override bool Equals(object poly2)
        {
            return !(this != (MyPolynom)poly2);
        }

        public override int GetHashCode()
        {
            StringBuilder str = new StringBuilder();

            for(int i=0;i<coefficients.Length;i++)
            {
                str.Append(coefficients[i].ToString()+"|");
            }

            return str.GetHashCode();
        }

        public override string ToString()
        {
            StringBuilder s = new StringBuilder();

            for(int i=coefficients.Length-1;i>0;i--) s.Append(Math.Round(this[i],3)+"*x^"+i+" + ");

            s.Append(Math.Round(this[0], 3));

            return s.ToString();
        }
    }
}

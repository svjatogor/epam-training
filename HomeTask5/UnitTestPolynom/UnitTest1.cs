﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Polynom;

namespace UnitTestProject1
{
    [TestClass]
    public class UnitTest1
    {
        private bool Equals(int[] a, int[] b)
        {
            if (a.Length != b.Length) return false;

            for (int i = 0; i < a.Length; i++)
            {
                if (a[i] != b[i]) return false;
            }

            return true;
        }

        [TestMethod]
        public void TestMethod3()
        {
            MyPolynom poly1 = new MyPolynom(3);
            poly1.Data = new double[] { 1, 2, 3 };

            double expected = 34;
            double actual = poly1.DependsOf(3);

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestMethod4()
        {
            MyPolynom poly1 = new MyPolynom(3);
            poly1.Data = new double[] { 1, 2, 3 };
            MyPolynom poly2 = new MyPolynom(3);
            poly2.Data = new double[] { 1, 2, 3 };

            Assert.AreEqual(poly1,poly2);
        }


    }
}

﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Vector;

namespace UnitTestVector
{
    [TestClass]
    public class UnitTest1
    {
        private new bool Equals(int[] a, int[] b)
        {
            if (a.Length != b.Length) return false;

            for(int i=0;i<a.Length;i++)
            {
                if (a[i] != b[i]) return false;
            }

            return true;
        }

        [TestMethod]
        public void TestMethod1()
        {
            MyVector v1 = new MyVector(3), v2 = new MyVector(3);

            int[] a1 = new int[] { 1, 2, 3 };
            int[] a2 = new int[] { 4, 5, 6 };

            int[] aExp = new int[] { 5, 7, 9 };

            v1.Data = a1;
            v2.Data = a2;

            MyVector v3 = v1 + v2;

            bool actual = Equals(aExp, v3.Data);
            bool expected = true;

            Assert.AreEqual(expected, actual, "Не совпадают");
        }

        [TestMethod]
        public void TestMethod2()
        {
            MyVector v1 = new MyVector(3), v2 = new MyVector(3);

            int[] a1 = new int[] { 1, 2, 3 };
            int[] a2 = new int[] { 4, 5, 6 };

            int[] aExp = new int[] { -3,-3,-3 };

            v1.Data = a1;
            v2.Data = a2;

            MyVector v3 = v1 - v2;

            bool actual = Equals(aExp, v3.Data);
            bool expected = true;

            Assert.AreEqual(expected, actual, "Не совпадают");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestMethod5()
        {
            MyVector v = new MyVector(5);
        }


    }
}

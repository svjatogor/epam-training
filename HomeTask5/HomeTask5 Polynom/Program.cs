﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Polynom;

namespace HomeTask5_Polynom
{
    class Program
    {
        public static MyPolynom CreatePolynom()
        {
            int maxStepen;

            Console.Write("Введите максимальную степень полинома: ");
            string maxStepenStr = Console.ReadLine();
            maxStepen = Convert.ToInt32(maxStepenStr);

            MyPolynom poly1 = new MyPolynom(maxStepen);

            for (int i = 0; i < maxStepen; i++)
            {
                Console.Write("Введите " + i + "-й коэффициент: ");
                string coefStr = Console.ReadLine();
                poly1[i] = Convert.ToDouble(coefStr);
            }

            Console.WriteLine("Получен полином: " + poly1.ToString());
            Console.WriteLine("--------------------------------------");

            return poly1;
        }

        static void Main(string[] args)
        {
            MyPolynom poly1 = CreatePolynom(),poly2 = CreatePolynom();

            Console.WriteLine("Полиномы "+((poly1!=poly2)?"не ":"") + "равны");

            Console.Write("Введите число для расчёта: ");
            string xValueStr = Console.ReadLine();
            double x = Convert.ToDouble(xValueStr);

            Console.WriteLine("f1(x) = "+poly1.DependsOf(x));
        }
    }
}

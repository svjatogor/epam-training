﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vector
{
    public class MyVector
    {
        private int[] vector;
        private static int numOfVectors = 0;

        public MyVector(int numOfElements)
        {
            if (numOfElements < 0) throw new ArgumentException("Отрицательная длина вектора");
            vector = new int[numOfElements];
        }

        static MyVector()
        {
            numOfVectors++;
        }

        public MyVector(MyVector obj)
        {
            vector = (int[])obj.vector.Clone();
        }

        public int[] Data
        {
            get {
                int[] newVector = new int[vector.Length];
                newVector = (int[])vector.Clone();

                return newVector;
            }
            set {
                if (value == null) throw new ArgumentException("Передан null агрумент");

                //Array.Copy(value,vector,value.Length);
                vector = (int[])value.Clone();
            }
        }

        public static MyVector operator +(MyVector vector1, MyVector vector2)
        {
            if (vector1 == null || vector2 == null) throw new ArgumentNullException("Передан NULL");
            
            int[] data2 = vector2.Data;

            MyVector newV = new MyVector(vector1);

            if (newV.Data.Length != data2.Length) throw new ArgumentException("Переданы массивы разной длины");

            for (int i = 0; i < newV.Data.Length; i++)
            {
                newV[i] += data2[i];
            }

            return newV;
        }

        public static MyVector operator -(MyVector vector1, MyVector vector2)
        {
            if (vector1 == null || vector2 == null) throw new ArgumentNullException("Передан NULL");

            int[] data2 = vector2.Data;

            MyVector newV = new MyVector(vector1);

            if (newV.Data.Length != data2.Length) throw new ArgumentException("Переданы массивы разной длины");

            for (int i = 0; i < newV.Data.Length; i++)
            {
                newV[i] -= data2[i];
            }

            return newV;
        }

        public static double operator *(MyVector vector1, MyVector vector2)
        {
            if (vector1 == null || vector2 == null) throw new ArgumentNullException("Передан NULL");

            int[] data1 = vector1.Data;
            int[] data2 = vector2.Data;
            double proizv = 0;

            if (data1.Length != data2.Length) throw new ArgumentException("Переданы массивы разной длины");

            for (int i = 0; i < data1.Length; i++)
            {
                proizv += data1[i]*data2[i];
            }

            return proizv;    
        }

        public double Length
        {
            get
            {
                double result = 0;

                for(int i=0;i<vector.Length;i++)
                {
                    result += vector[i] * vector[i];
                }

                return Math.Sqrt(result);
            }
        }

        public static double Angle(MyVector vector1, MyVector vector2)
        {
            if (vector1 == null || vector2 == null) throw new ArgumentNullException("Передан NULL");
            if (vector1.Data.Length != vector2.Data.Length) throw new ArgumentException("Переданы векторы разной длины");

            return Math.Acos((vector1*vector2)/(vector1.Length*vector2.Length));
        }

        public int this[int index]
        {
            get
            {
                if (index >= vector.Length || index < 0) throw new ArgumentOutOfRangeException("Нет такого индекса в векторе");

                return vector[index];
            }
            set
            {
                if (index >= vector.Length || index < 0) throw new ArgumentOutOfRangeException("Нет такого индекса в векторе");

                vector[index] = value;
            }
        }

        ~MyVector()
        {
            numOfVectors--;
        }
    }
}

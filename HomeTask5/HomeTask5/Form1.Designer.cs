﻿namespace HomeTask5
{
    partial class Form1
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.vector1Input = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.vector2Input = new System.Windows.Forms.TextBox();
            this.processButton = new System.Windows.Forms.Button();
            this.vectorsBox = new System.Windows.Forms.ComboBox();
            this.coordsBox = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.coordValue = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.add_vectors = new System.Windows.Forms.Button();
            this.sumResult = new System.Windows.Forms.TextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.subResult = new System.Windows.Forms.TextBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.multipleButton = new System.Windows.Forms.Button();
            this.multipleResult = new System.Windows.Forms.TextBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.angleBox = new System.Windows.Forms.ComboBox();
            this.getAngle = new System.Windows.Forms.Button();
            this.angleResult = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.SuspendLayout();
            // 
            // vector1Input
            // 
            this.vector1Input.Location = new System.Drawing.Point(46, 55);
            this.vector1Input.Name = "vector1Input";
            this.vector1Input.Size = new System.Drawing.Size(260, 20);
            this.vector1Input.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(46, 39);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(125, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Координаты вектора 1:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(46, 81);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(125, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Координаты вектора 2:";
            // 
            // vector2Input
            // 
            this.vector2Input.Location = new System.Drawing.Point(46, 97);
            this.vector2Input.Name = "vector2Input";
            this.vector2Input.Size = new System.Drawing.Size(260, 20);
            this.vector2Input.TabIndex = 2;
            // 
            // processButton
            // 
            this.processButton.Location = new System.Drawing.Point(46, 123);
            this.processButton.Name = "processButton";
            this.processButton.Size = new System.Drawing.Size(260, 23);
            this.processButton.TabIndex = 4;
            this.processButton.Text = "Обработать вектора";
            this.processButton.UseVisualStyleBackColor = true;
            this.processButton.Click += new System.EventHandler(this.processButton_Click);
            // 
            // vectorsBox
            // 
            this.vectorsBox.FormattingEnabled = true;
            this.vectorsBox.Items.AddRange(new object[] {
            "1",
            "2"});
            this.vectorsBox.Location = new System.Drawing.Point(11, 38);
            this.vectorsBox.Name = "vectorsBox";
            this.vectorsBox.Size = new System.Drawing.Size(43, 21);
            this.vectorsBox.TabIndex = 5;
            this.vectorsBox.SelectionChangeCommitted += new System.EventHandler(this.vectorsBox_SelectionChangeCommitted);
            // 
            // coordsBox
            // 
            this.coordsBox.FormattingEnabled = true;
            this.coordsBox.Location = new System.Drawing.Point(60, 38);
            this.coordsBox.Name = "coordsBox";
            this.coordsBox.Size = new System.Drawing.Size(64, 21);
            this.coordsBox.TabIndex = 6;
            this.coordsBox.SelectionChangeCommitted += new System.EventHandler(this.coordsBox_SelectionChangeCommitted);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(11, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(43, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Вектор";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(57, 22);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(67, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Координата";
            // 
            // coordValue
            // 
            this.coordValue.Location = new System.Drawing.Point(130, 39);
            this.coordValue.Name = "coordValue";
            this.coordValue.Size = new System.Drawing.Size(141, 20);
            this.coordValue.TabIndex = 9;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(127, 23);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(119, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "Значение координаты";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.processButton);
            this.groupBox1.Controls.Add(this.vector1Input);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.vector2Input);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(347, 178);
            this.groupBox1.TabIndex = 11;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Ввод информации о векторе";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.coordValue);
            this.groupBox2.Controls.Add(this.vectorsBox);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.coordsBox);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Location = new System.Drawing.Point(365, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(283, 74);
            this.groupBox2.TabIndex = 12;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Узнать значение координаты";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.add_vectors);
            this.groupBox3.Controls.Add(this.sumResult);
            this.groupBox3.Location = new System.Drawing.Point(365, 92);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(283, 98);
            this.groupBox3.TabIndex = 13;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Сложение векторов";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(8, 48);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(112, 13);
            this.label6.TabIndex = 6;
            this.label6.Text = "Результат сложения";
            // 
            // add_vectors
            // 
            this.add_vectors.Location = new System.Drawing.Point(11, 19);
            this.add_vectors.Name = "add_vectors";
            this.add_vectors.Size = new System.Drawing.Size(260, 23);
            this.add_vectors.TabIndex = 5;
            this.add_vectors.Text = "Сложение векторов";
            this.add_vectors.UseVisualStyleBackColor = true;
            this.add_vectors.Click += new System.EventHandler(this.add_vectors_Click);
            // 
            // sumResult
            // 
            this.sumResult.Location = new System.Drawing.Point(11, 65);
            this.sumResult.Name = "sumResult";
            this.sumResult.Size = new System.Drawing.Size(260, 20);
            this.sumResult.TabIndex = 5;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label7);
            this.groupBox4.Controls.Add(this.button2);
            this.groupBox4.Controls.Add(this.subResult);
            this.groupBox4.Location = new System.Drawing.Point(365, 196);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(283, 98);
            this.groupBox4.TabIndex = 14;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Вычитание векторов";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(8, 48);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(116, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "Результат вычитания";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(11, 19);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(260, 23);
            this.button2.TabIndex = 5;
            this.button2.Text = "Вычитание векторов";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // subResult
            // 
            this.subResult.Location = new System.Drawing.Point(11, 65);
            this.subResult.Name = "subResult";
            this.subResult.Size = new System.Drawing.Size(260, 20);
            this.subResult.TabIndex = 5;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.label8);
            this.groupBox5.Controls.Add(this.multipleButton);
            this.groupBox5.Controls.Add(this.multipleResult);
            this.groupBox5.Location = new System.Drawing.Point(12, 196);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(347, 98);
            this.groupBox5.TabIndex = 15;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Умножение векторов";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(46, 48);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(119, 13);
            this.label8.TabIndex = 6;
            this.label8.Text = "Результат умножения";
            // 
            // multipleButton
            // 
            this.multipleButton.Location = new System.Drawing.Point(46, 19);
            this.multipleButton.Name = "multipleButton";
            this.multipleButton.Size = new System.Drawing.Size(260, 23);
            this.multipleButton.TabIndex = 5;
            this.multipleButton.Text = "Умножение векторов";
            this.multipleButton.UseVisualStyleBackColor = true;
            this.multipleButton.Click += new System.EventHandler(this.multipleButton_Click);
            // 
            // multipleResult
            // 
            this.multipleResult.Location = new System.Drawing.Point(46, 64);
            this.multipleResult.Name = "multipleResult";
            this.multipleResult.Size = new System.Drawing.Size(260, 20);
            this.multipleResult.TabIndex = 5;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.angleBox);
            this.groupBox6.Controls.Add(this.getAngle);
            this.groupBox6.Controls.Add(this.angleResult);
            this.groupBox6.Location = new System.Drawing.Point(12, 300);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(636, 52);
            this.groupBox6.TabIndex = 16;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Угол между векторами";
            // 
            // angleBox
            // 
            this.angleBox.FormattingEnabled = true;
            this.angleBox.Items.AddRange(new object[] {
            "Градусы",
            "Радианы"});
            this.angleBox.Location = new System.Drawing.Point(14, 22);
            this.angleBox.Name = "angleBox";
            this.angleBox.Size = new System.Drawing.Size(125, 21);
            this.angleBox.TabIndex = 8;
            // 
            // getAngle
            // 
            this.getAngle.Location = new System.Drawing.Point(146, 23);
            this.getAngle.Name = "getAngle";
            this.getAngle.Size = new System.Drawing.Size(150, 20);
            this.getAngle.TabIndex = 7;
            this.getAngle.Text = "Узнать угол";
            this.getAngle.UseVisualStyleBackColor = true;
            this.getAngle.Click += new System.EventHandler(this.getAngle_Click);
            // 
            // angleResult
            // 
            this.angleResult.Location = new System.Drawing.Point(302, 23);
            this.angleResult.Name = "angleResult";
            this.angleResult.Size = new System.Drawing.Size(322, 20);
            this.angleResult.TabIndex = 0;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(656, 360);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "Работа с вектором";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox vector1Input;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox vector2Input;
        private System.Windows.Forms.Button processButton;
        private System.Windows.Forms.ComboBox vectorsBox;
        private System.Windows.Forms.ComboBox coordsBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox coordValue;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button add_vectors;
        private System.Windows.Forms.TextBox sumResult;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox subResult;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button multipleButton;
        private System.Windows.Forms.TextBox multipleResult;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.ComboBox angleBox;
        private System.Windows.Forms.Button getAngle;
        private System.Windows.Forms.TextBox angleResult;
    }
}


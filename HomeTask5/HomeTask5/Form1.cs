﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Vector;

namespace HomeTask5
{
    public partial class Form1 : Form
    {
        private MyVector vector1 = new MyVector(1),vector2 = new MyVector(1);

        public Form1()
        {
            InitializeComponent(); 
        }

        private void processButton_Click(object sender, EventArgs e)
        {
            try
            {
                //Получаем координаты векторов
                int[] vect1Coords = vector1Input.Text.GetNums();
                int[] vect2Coords = vector2Input.Text.GetNums();

                //Инициализируем вектора
                vector1 = new MyVector(vect1Coords.Length);
                vector2 = new MyVector(vect2Coords.Length);
                vector1.Data = vect1Coords;
                vector2.Data = vect2Coords;

                MessageBox.Show("Загружено два вектора (длиной " + vector1.Data.Length + " и " + vector2.Data.Length + ")", "Вектора загружены");
            }
            catch (Exception exp)
            {
                MessageBox.Show(exp.Message,"Произошла ошибка");
            }
        }

        private void vectorsBox_SelectionChangeCommitted(object sender, EventArgs e)
        {
            int Length = (vectorsBox.Items[vectorsBox.SelectedIndex].ToString()=="1")?vector1.Data.Length:vector2.Data.Length;
            coordsBox.Items.Clear();
            coordValue.Text = "";

            for (int i = 0; i < Length; i++) coordsBox.Items.Add(i.ToString());
        }

        private void coordsBox_SelectionChangeCommitted(object sender, EventArgs e)
        {
            coordValue.Text = ((vectorsBox.Text == "1") ? vector1[coordsBox.SelectedIndex] : vector2[coordsBox.SelectedIndex]).ToString();
        }

        private void add_vectors_Click(object sender, EventArgs e)
        {
            MyVector v = vector1 + vector2;

            sumResult.Text = v.Data.GetString();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            MyVector v = vector1 - vector2;

            subResult.Text = v.Data.GetString();
        }

        private void multipleButton_Click(object sender, EventArgs e)
        {
            double v = vector1 * vector2;

            multipleResult.Text = v.ToString();
        }

        private void getAngle_Click(object sender, EventArgs e)
        {
            if (angleBox.SelectedIndex == 0) angleResult.Text = (MyVector.Angle(vector1, vector2)*180/Math.PI).ToString();
            else angleResult.Text = MyVector.Angle(vector1, vector2).ToString();
        }

        private void Form1_MouseUp(object sender, MouseEventArgs e)
        {

        }
    }

    public static class Expander
    {
        public static int[] GetNums(this String s)
        {
            string[] ArrS = s.Split(',', ' ');
            int[] nums = new int[ArrS.Length];
            int i = 0;

            foreach (String str in ArrS)
            {
                nums[i] = Convert.ToInt32(str);
                i++;
            }

            return nums;
        }

        public static String GetString(this int[] nums)
        {
            StringBuilder s = new StringBuilder();

            for (int i = 0; i < nums.Length; i++)
                s.Append(nums[i].ToString() + ", ");

            s.Remove(s.Length - 2, 2);

            return s.ToString();
        }
    }
}

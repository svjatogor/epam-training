﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            StreamReader file = new StreamReader("input.txt");
            Console.WriteLine("Текущая позиция: " + file.BaseStream.Position);
            string s = file.ReadLine();
            Console.WriteLine("Текущая позиция: " + file.BaseStream.Seek(0,SeekOrigin.Current));
            s = file.ReadLine();
            Console.WriteLine("Текущая позиция: " + file.BaseStream.Position);

        }
    }
}

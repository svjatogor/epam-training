﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProgConv
{
    public class ProgramConverter:IConvertible
    {
        public string ConvertToCSharp(string inpCode)
        {
            if (inpCode == null) throw new ArgumentNullException("Передан null вместо строки");
            return "C# - " + inpCode;
        }

        public string ConvertToVB(string inpCode)
        {
            if (inpCode == null) throw new ArgumentNullException("Передан null вместо строки");
            return "VB - " + inpCode;
        }
    }
}

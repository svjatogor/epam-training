﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProgConv
{
    public class ProgramHelper : ProgramConverter, ICodeChecker
    {
        /// <summary>
        /// Проверяет синтаксис переданного кода на выбранном языке программирования
        /// </summary>
        /// <param name="inpCode">Код программы для проверки</param>
        /// <param name="language">Язык проверяемого кода (C# или VB)</param>
        /// <returns></returns>
        public bool CodeCheckSyntax(string inpCode, string language)
        {
            if (inpCode == null || language == null) throw new ArgumentNullException("Ошибка. Передан null");
            if (language == "C#") return (inpCode.Substring(0, 2) == "C#");
            else if (language == "VB") return (inpCode.Substring(0, 2) == "VB");
            return false;
        }
    }
}

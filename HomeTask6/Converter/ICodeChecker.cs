﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProgConv
{
    public interface ICodeChecker
    {
        bool CodeCheckSyntax(string inpCode, string language);
    }
}

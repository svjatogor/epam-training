﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProgConv
{
    public interface IConvertible
    {
        string ConvertToCSharp(string inpCode);
        string ConvertToVB(string inpCode);
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MyReader;
using System.IO;

namespace HomeTask_Z2
{
    public partial class Form1 : Form
    {
        StrReader newFile;

        public Form1()
        {
            InitializeComponent();
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            openFile.ShowDialog();
            textBox1.Text = textBox2.Text = "";
        }

        private void openFile_FileOk(object sender, CancelEventArgs e)
        {
            button1.Enabled = true;
            textBox2.Enabled = true;
            label4.Text = openFile.FileName;

            newFile = new StrReader(openFile.FileName);

            label2.Text = newFile.Position.ToString() + "/" + newFile.Length.ToString();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            textBox1.Text += newFile.ReadNextBlock((int)newFile.Length / 10);
            if (newFile.Length < 10) textBox1.Text += newFile.ReadNextBlock((int)(newFile.Length));
            if (newFile.EndOfStream) button2.Enabled = false;
            
            progressBar1.Value = (int)Math.Round(newFile.Position / (double)newFile.Length * 100);
            label2.Text = newFile.Position.ToString() + "/" + newFile.Length.ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox2.Text == "qwerty")
            {
                if (!newFile.EndOfStream) button2.Enabled = true;
            }
            else MessageBox.Show("Неверный пароль");
        }
    }
}

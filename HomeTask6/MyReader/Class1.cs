﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace MyReader
{
    public class StrReader : StreamReader
    {
        private string path;
        private long position;

        public StrReader(string path):base(path)
        {
            if (path == null) throw new ArgumentNullException("Передан null");
            this.path = path;
        }

        /// <summary>
        /// Текущая позиция в файле
        /// </summary>
        public long Position
        {
            get
            {
                //return BaseStream.Position;
                //return this.BaseStream.Seek(0, SeekOrigin.Current);
                return position;
            }
        }

        /// <summary>
        /// Длина файла
        /// </summary>
        public long Length
        {
            get
            {
                return this.BaseStream.Length;
            }
        }

        /// <summary>
        /// Считывает следующий блок данных из файла
        /// </summary>
        /// <param name="blockLength">Длина блока (количество символов)</param>
        /// <returns>Возвращает строку (считанный блок данных)</returns>
        public string ReadNextBlock(int blockLength)
        {
            if (blockLength < 0) throw new ArgumentException("Отрицательная длина блока данных");

            char[] buffer = new char[blockLength];

            position += ReadBlock(buffer, 0, blockLength);

            return new string(buffer);
        }

        /// <summary>
        /// Закрывает файловый поток
        /// </summary>
        public override void Close()
        {
            base.Close();
        }

        /// <summary>
        /// Считывает символ из входного потока и перемещает указатель на одну позицию вперёд
        /// </summary>
        /// <returns>Возвращает значение символа</returns>
        public override int Read()
        {
            int c = base.Read();
            if (c != 0) position++;
            return c;
        }

        /// <summary>
        /// Считывает строку из входного потока
        /// </summary>
        /// <returns></returns>
        public override string ReadLine()
        {
            string s = base.ReadLine();
            position += s.Length;
            return s;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProgConv;

namespace HomeTask6_Z1
{
    class Program
    {
        static void Main(string[] args)
        {
            ProgramConverter[] mas = new ProgramConverter[5] {new ProgramConverter(),new ProgramHelper(), new ProgramHelper(), new ProgramConverter(),new ProgramHelper()};

            foreach(ProgramConverter ind in mas)
            {
                try
                {
                    Console.WriteLine("Объект "+ind.GetType()+":");

                    string code = "Program Code for conversion";

                    string newCSharpCode = ind.ConvertToCSharp(code);
                    string newVBCode = ind.ConvertToVB(code);

                    if (!(ind is ProgConv.ICodeChecker)) Console.WriteLine(ind.GetType() + " не поддерживает ICodeChecker");

                    if ((ind as ProgConv.ICodeChecker).CodeCheckSyntax(code, "C#")) Console.WriteLine("Код \"{0}\" написан на C#", code);
                    if ((ind as ProgConv.ICodeChecker).CodeCheckSyntax(newCSharpCode, "C#")) Console.WriteLine("Код \"{0}\" написан на C#", newCSharpCode);
                    if ((ind as ProgConv.ICodeChecker).CodeCheckSyntax(newVBCode, "VB")) Console.WriteLine("Код \"{0}\" написан на VB", newVBCode);
                }
                catch (Exception exp)
                {
                    Console.WriteLine("Произошла ошибка: " + exp.Message);
                }
                finally
                {
                    Console.WriteLine("----------------------------\n");
                }
            }
        }
    }
}
